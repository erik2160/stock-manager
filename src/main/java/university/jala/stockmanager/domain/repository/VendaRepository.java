package university.jala.stockmanager.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import university.jala.stockmanager.domain.model.Venda;

@Repository
public interface VendaRepository extends JpaRepository<Venda, Long> {
}
