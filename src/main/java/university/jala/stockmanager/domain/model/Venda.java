package university.jala.stockmanager.domain.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Venda {
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_venda;

    @Column(name = "fk_id_produto")
    private Long id_produto;

    @NotNull
    private int quantidade;

    @Column(columnDefinition = "TIMESTAMP", nullable = false)
    private LocalDateTime data;

    @PrePersist
    protected void onCreate() {
        data = LocalDateTime.now();
    }
}
