package university.jala.stockmanager.service;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import university.jala.stockmanager.domain.model.Produto;
import university.jala.stockmanager.domain.model.Venda;
import university.jala.stockmanager.domain.repository.ProdutoRepository;
import university.jala.stockmanager.domain.repository.VendaRepository;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class VendaService {

    private final VendaRepository vendaRepository;
    private final ProdutoRepository produtoRepository;
    public ResponseEntity<List<Venda>> listarVendas() {
        if (vendaRepository.findAll().isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok(vendaRepository.findAll());
        }
    }

    public ResponseEntity<Venda> venderProduto(Venda venda) {

        if (venda == null || venda.getId_produto() == null) {
            return ResponseEntity.badRequest().build();
        }

        Long produtoId = venda.getId_produto();

        Optional<Produto> produtoOptional = produtoRepository.findById(produtoId);

        if (produtoOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        } else {
            Produto produto = produtoOptional.get();

            int quantidadeVenda = venda.getQuantidade();
            int estoqueProduto = produto.getEstoque();

            if (hasEstoque(produto, quantidadeVenda) && !validQuantity(quantidadeVenda)) {
                produto.setEstoque(estoqueProduto - quantidadeVenda);

                return ResponseEntity.ok(vendaRepository.save(venda));
            }
            return ResponseEntity.badRequest().build();
        }
    }

    public boolean hasEstoque(Produto produto, int quantidadeVenda) {
        return quantidadeVenda <= produto.getEstoque();
    }

    public boolean validQuantity(int quantity) {
        return quantity <= 0;
    }
}
