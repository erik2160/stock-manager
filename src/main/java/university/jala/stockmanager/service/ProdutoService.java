package university.jala.stockmanager.service;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import university.jala.stockmanager.domain.model.Produto;
import university.jala.stockmanager.domain.repository.ProdutoRepository;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class ProdutoService {
    private final ProdutoRepository produtoRepository;

    public ResponseEntity<List<Produto>> listarProdutos() {
        if (produtoRepository.findAll().isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok(produtoRepository.findAll());
        }
    }

    @Transactional
    public ResponseEntity<Produto> inserirProduto(Produto produto) {

        if (produto == null || produto.getPreco() <= 0) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(produtoRepository.save(produto));
    }

    @Transactional
    public ResponseEntity<Produto> atualizarProduto(Long produtoId, Produto produto) {
        if (!produtoRepository.existsById(produtoId)) {
            return ResponseEntity.notFound().build();
        }

        produto.setId_produto(produtoId);
        return ResponseEntity.ok(produtoRepository.save(produto));
    }

    public ResponseEntity<Produto> buscarProduto(Long produtoId) {
        Optional<Produto> produto = produtoRepository.findById(produtoId);
        return produto.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    public ResponseEntity<HttpStatus> removerProduto(Long produtoId) {
        if (!produtoRepository.existsById(produtoId)) {
            return ResponseEntity.notFound().build();
        } else {
            produtoRepository.deleteById(produtoId);
            return ResponseEntity.noContent().build();
        }
    }
}