package university.jala.stockmanager.controller;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import university.jala.stockmanager.domain.model.Produto;
import university.jala.stockmanager.service.ProdutoService;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    private final ProdutoService produtoService;

    @GetMapping
    public ResponseEntity<List <Produto>> listar() {
        return produtoService.listarProdutos();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public ResponseEntity<Produto> inserir(@Valid @RequestBody Produto produto) {
        return produtoService.inserirProduto(produto);
    }

    @PutMapping("/{produtoId}")
    public ResponseEntity<Produto> atualizar(@Valid @PathVariable Long produtoId, @RequestBody Produto produto) {
        return produtoService.atualizarProduto(produtoId, produto);
    }

    @DeleteMapping("/{produtoId}")
    public ResponseEntity<HttpStatus> remover(@PathVariable Long produtoId) {
        return produtoService.removerProduto(produtoId);
    }

    @GetMapping("/{produtoId}")
    public ResponseEntity<Produto> buscar(@PathVariable Long produtoId) {
        return produtoService.buscarProduto(produtoId);
    }
}
