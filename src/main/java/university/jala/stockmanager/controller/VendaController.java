package university.jala.stockmanager.controller;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import university.jala.stockmanager.domain.model.Produto;
import university.jala.stockmanager.domain.model.Venda;
import university.jala.stockmanager.service.VendaService;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/vendas")
public class VendaController {

    private final VendaService vendaService;

    @RequestMapping
    public ResponseEntity<List<Venda>> listar() {
        return vendaService.listarVendas();
    }

    @PostMapping()
    public ResponseEntity<Venda> vender(@RequestBody Venda venda) {
        return vendaService.venderProduto(venda);
    }
}