CREATE TABLE venda (
    id_venda bigint not null auto_increment,
    fk_id_produto bigint,
    quantidade int not null,
    data timestamp default current_timestamp,

    primary key (id_venda),
    foreign key (fk_id_produto) references produto(id_produto)
);