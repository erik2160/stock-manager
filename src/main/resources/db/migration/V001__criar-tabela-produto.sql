CREATE TABLE produto (
    id_produto bigint not null auto_increment,
    nome varchar(50) not null,
    preco double not null,
    estoque int not null,

    primary key (id_produto)
);